const {mkdirSync, existsSync} = require('fs');
const moment = require('moment-timezone');

const getDate = () => {
    return moment().format("YYYY-MM-DD");
};
const getTime = () => {
    return moment().format("HH:mm:ss");
};
const getDateTime = () => {
    return moment().format("YYYY-MM-DD HH:mm:ss");
};

module.exports.getDate = getDate;
module.exports.getTime = getTime;
module.exports.getDateTime = getDateTime;

module.exports.checkAndCreateDirectory = (path) => {
    if (!existsSync(path)) {
        mkdirSync(path);
    }
};
