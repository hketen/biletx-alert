const {getDateTime} = require('./utils');
const emailNotification = require('./email-notification');

const {TO_EMAILS, PAGE_REFRESH_MINUTES, RECHECK_MINUTES} = process.env;

const sleep = (minute) => new Promise((resolve) => setTimeout(resolve, minute * 60 * 1000));

module.exports = async ({PAGE_URL, browser}) => {

    const page = await browser.newPage();

    await page.goto(PAGE_URL);

    await page.setViewport({
        width: 1920,
        height: 1080
    });

    page.exists = async (selector) => page.evaluate((s) => !!document.querySelector(s), [selector]);

    page.saveSS = async () => {

        await page.screenshot({path: `tmp/${getDateTime()}.png`});
    }

    page.accept = async () => {

        console.log("Accepting cookies...");

        const acceptId = "#_evidon-accept-button";
        const accept = await page.exists(acceptId);

        if (accept) {

            await page.click(acceptId, {delay: 5000})
        }
    };

    page.checkTicket = async () => {

        console.log("Have a new ticket? Please wait! I am checking... ");

        const statuses = await page.evaluate(() => {

            const elements = document.querySelectorAll(".omega.eventGroupListItem1b .ln2")

            const status = [];

            for (let i = 0; i < elements.length; i++) {
                status.push(elements[i].innerText);
            }
            return status;
        });

        const hasStatus = statuses.some(status => status.toLowerCase().indexOf("sat") >= 0);

        console.log(`I checked! ${getDateTime()}`);
        if (hasStatus) {

            console.log("Yay!! There is a new ticket!");

            await Promise.all([
                page.saveSS(),
                emailNotification({
                    toEmails: TO_EMAILS,
                    URL: PAGE_URL
                })
            ]);

            return true;
        }
        else {

            console.log("Sorry! There is not a new ticket! I will check later.");

            return false;
        }
    };

    page.checker = async () => {

        console.log("reloading page...");
        await page.reload();
        const hasNewTicket = await page.checkTicket();

        await sleep(hasNewTicket ? RECHECK_MINUTES : PAGE_REFRESH_MINUTES);

        await page.checker();
    };

    return page;
};
