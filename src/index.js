require("./exit-handler");

require('dotenv').config();

const puppeteer = require('puppeteer');
const {checkAndCreateDirectory} = require('./utils');
const biletx = require('./biletx');

const [, , PAGE_URL] = process.argv;

(async () => {

    checkAndCreateDirectory("tmp");

    const browser = await puppeteer.launch({
            args: [
                '--disable-gpu',
                '--disable-setuid-sandbox',
                '--force-device-scale-factor',
                '--ignore-certificate-errors',
                '--no-sandbox',
                '--disable-dev-shm-usage'
            ],
            defaultViewport: null,
            devtools: false,
            ignoreHTTPSErrors: true,
            handleSIGINT: false
        }
    );

    const page = await biletx({
        browser,
        PAGE_URL
    });

    await page.accept();

    await page.checkTicket();

    await page.checker();
})();
