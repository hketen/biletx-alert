const {isString} = require('lodash');
const nodemailer = require('nodemailer');

const {EMAIL_AUTH_USER, EMAIL_AUTH_PASS} = process.env;

module.exports = async ({toEmails, URL}) => {

    // create reusable transporter object using the default SMTP transport
    const transporter = nodemailer.createTransport({
        host: "smtp.yandex.ru",
        // port: 587,
        // secure: false,
        port: 465,
        secure: true,
        auth: {
            user: EMAIL_AUTH_USER,
            pass: EMAIL_AUTH_PASS
        },
    });

    // send mail with defined transport object
    const info = await transporter.sendMail({
        from: `"BiletX 👻" <${EMAIL_AUTH_USER}>`,
        to: isString(toEmails) ? toEmails : toEmails.join(' '),
        subject: "Yayyy!!!!",
        html: `
            <b>BiletX Bileti Satışta!!</b>
            <b>${URL}</b>
        `
    });

    console.log("Message Sent Info: %s", info.response);

    return info;
};
